# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import user
from . import config
from . import calls
from . import test_configuration

__all__ = ['register']


def register():
    Pool.register(
        calls.CallLog,
        calls.Caller,
        user.User,
        config.Configuration,
        test_configuration.TestConfigurationStart,
        module='tryphone', type_='model')
    Pool.register(
        test_configuration.TestConfiguration,
        module='tryphone', type_='wizard')
    Pool.register(
        module='tryphone', type_='report')
