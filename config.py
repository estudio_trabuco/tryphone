# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSingleton, ModelSQL, ModelView, fields

from providers.config import ProviderConfiguration
from providers.test import TestProvider
from providers.freetwitch import FreetwitchProvider


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Tryphone Configuration'
    __name__ = 'tryphone.configuration'

    host = fields.Char('Host', required=True)
    port = fields.Integer('Port', required=True)
    api_client = fields.Selection(
        [
            ('freetwitch', 'FreeTwitch'),
            ('test', 'Test')
        ],
        'Api Client',
        help="Client library to use"
    )
    api_password = fields.Char('Api Password')

    def get_provider(self):
        configuration = self._to_provider_configuration()
        providers = {
            'test': TestProvider(configuration),
            'freetwich': FreetwitchProvider(configuration)
        }
        return providers.get(self.api_client)

    def _to_provider_configuration(self):
        provider_configuration = ProviderConfiguration()
        provider_configuration.with_('host', str(self.host))
        provider_configuration.with_('port', self.port)
        provider_configuration.with_('password', self.api_password)
        return provider_configuration
