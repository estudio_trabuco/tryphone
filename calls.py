# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import datetime

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool


class CallLog(ModelView, ModelSQL):
    'Tryphone call log'
    __name__ = 'tryphone.call_log'

    user = fields.Many2One('res.user', 'User', required=True)
    phone_extension = fields.Char('Phone extension')
    destination = fields.Char('Destination')
    reference_id = fields.Char('Reference Id', help="Call Id on provider")
    date_time = fields.DateTime('Date Time', required=True)


class Caller(ModelView):
    'Tryphone caller'

    __name__ = 'tryphone.caller'

    @classmethod
    def call(cls, user, destination):
        pool = Pool()
        Configuration = pool.get('tryphone.configuration')
        configuration = Configuration(1)
        CallLog = pool.get('tryphone.call_log')

        provider = configuration.get_provider()
        call_log = CallLog()
        call_log.user = user
        call_log.phone_extension = user.phone_extension
        call_log.destination = destination
        reference_id = provider.call(
            origin=user.phone_extension,
            destination=destination
            )
        call_log.reference_id = reference_id
        call_log.date_time = datetime.today()
        call_log.save()
