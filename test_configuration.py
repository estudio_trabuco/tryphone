from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction


class TestConfigurationStart(ModelView):
    "Test configuration start form"

    __name__ = 'tryphone.test_configuration.start'

    to = fields.Char('To')


class TestConfiguration(Wizard):
    __name__ = 'tryphone.test_configuration'

    start = StateView('tryphone.test_configuration.start',
                      '',
                      [
                          Button(
                              "Cancel", 'end', 'tryton-cancel'),
                          Button(
                              "Import", 'try_call', 'tryton-ok', default=True)
                      ])
    try_call = StateTransition()

    def transition_try_call(self):
        pool = Pool()
        Caller = pool.get('tryphone.caller')
        User = pool.get('res.user')
        user = User(Transaction().user)
        Caller.call(
            user=user,
            destination=self.start.to
        )
        return 'end'
