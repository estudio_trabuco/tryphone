# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from .FreeTwitch.freeswitch_client import FreeswitchClient


class FreetwitchProvider:
    def __init__(self, provider_configuration):
        self.configuration = provider_configuration

    def call(self, origin, destination):
        self._init_client()
        try:
            self.client.authenticate(self.configuration.get('password'))
            self.client.call_echo_to_line(origin, 'echo ', destination)
        finally:
            self.client.close()
        return "freetwitch id"

    def _init_client(self):
        self.client = FreeswitchClient(
            self.configuration.get('host'),
            self.configuration.get('port')
        )
