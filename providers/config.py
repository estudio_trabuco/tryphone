# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

class ProviderConfiguration:
    _host = ''
    _port = ''
    _username = ''
    _password = ''

    def with_(self, property_, value):
        setattr(self, '_' + property_, value)

    def get(self, property_):
        getattr(self, '_' + property_)
