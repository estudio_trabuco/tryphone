class FreeSwitchConnectException(Exception):
    def __init__(self, message=''):
        basic_message = 'Error connecting to FreeSwitch Server.'
        self.message = " ".join([basic_message, message])


class FreeSwitchAuthException(Exception):
    def __init__(self, message=''):
        basic_message = 'Error authenticating on FreeSwitch Server.'
        self.message = " ".join([basic_message, message])
