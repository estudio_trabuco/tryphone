from .exceptions import FreeSwitchConnectException, FreeSwitchAuthException
from .client import Client
from .response_metadata import ResponseMetadata


class FreeswitchClient:
    string_control = '\n\n'

    def __init__(self, host, port):
        self.client = Client(host, port, self.string_control)
        self.__connect()

    def __connect(self):
        expected_connection_response = 'Content-Type: auth/request'
        response = self.client.connect(expected_connection_response)
        if not response == expected_connection_response:
            raise FreeSwitchConnectException(response)

    def authenticate(self, password):
        command = "auth"
        operation = " ".join([command, password])
        self.client.send(operation)
        response = self.client.getText()
        expected_response = "Content-Type: command/reply\n" \
            "Reply-Text: +OK accepted"
        if expected_response not in response:
            raise FreeSwitchAuthException(response)

    def _apiCommand(self, api_command):
        command = "api"
        operation = " ".join([command, api_command])
        self.client.send(operation)
        instruction_response = self.client.getText()
        response_metadata = ResponseMetadata.from_string(instruction_response)
        return self.client.getText(response_metadata.length)

    def uptime(self):
        return self._apiCommand('uptime')

    def call_echo_to_line(self, line, did_name='echo', did_number='0234'):
        user = 'user/' + str(line)
        return self._apiCommand(' '.join([
            'originate',
            user,
            '&echo()',
            'nose',
            'XML',
            did_name,
            did_number
        ]))

    def close(self):
        self.client.close()
