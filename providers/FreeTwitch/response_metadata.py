class ResponseMetadata:

    def __init__(self, _type, length):
        self._type = _type
        self.length = length

    @classmethod
    def from_string(self, string_response):
        lines = string_response.split('\n')
        _type = lines[0].split(": ")[1]
        length = int(lines[1].split(": ")[1])
        return ResponseMetadata(_type, length)
