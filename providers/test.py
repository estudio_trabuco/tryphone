# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

class TestProvider:
    def __init__(self, provider_configuration):
        self.provider_configuration = provider_configuration

    def call(self, origin, destination):
        return "test"
