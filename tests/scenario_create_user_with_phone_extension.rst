========================================
Crear un usuario con numero de extension
========================================

Dependencias::

  >>> from proteus import Model, Wizard
  >>> from trytond.tests.tools import activate_modules

Activación de modulos::

  >>> config = activate_modules('tryphone')

Usuarion con extensión::

  >>> User = Model.get('res.user')
  >>> user = User()
  >>> user.phone_extension = '120'
  >>> user.login = 'login'
  >>> user.save()
  >>> user.phone_extension
  '120'

