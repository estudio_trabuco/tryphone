=================================================
Manejo de registros de llamadas.
=================================================

Dependencias::

  >>> from datetime import datetime

  >>> from proteus import Model, Wizard
  >>> from trytond.tests.tools import activate_modules

Activación de modulos::

  >>> config = activate_modules('tryphone')

Crear registro de llamada::

  >>> User = Model.get('res.user')
  >>> users = User.find()
  >>> users[0].login
  'admin'
  >>> admin = users[0]

  >>> CallLog = Model.get('tryphone.call_log')
  >>> call_log = CallLog()
  >>> call_log.user = admin
  >>> call_log.destination = '23434'
  >>> call_log.date_time = datetime.today()
  >>> call_log.reference_id = 'uaxw4324sdf'
  >>> call_log.save()

  >>> call_log.user.login
  'admin'

