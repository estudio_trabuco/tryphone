=================================================
Configurar la PBX para poder ser usada en Tryton.
=================================================

Dependencias::

  >>> from proteus import Model, Wizard
  >>> from trytond.tests.tools import activate_modules

Activación de modulos::

  >>> config = activate_modules('tryphone')

Usuario con extensión::

  >>> ConfigPBX = Model.get('tryphone.configuration')
  >>> config_pbx = ConfigPBX()
  >>> config_pbx.host = '127.0.0.1'
  >>> config_pbx.port = 8706
  >>> config_pbx.api_client = 'test'
  >>> config_pbx.api_password = 'some_password'
  >>> config_pbx.save()
  >>> config_pbx.host
  '127.0.0.1'
  >>> config_pbx.port
  8706
  >>> config_pbx.api_client
  'test'
  >>> config_pbx.api_password
  'some_password'

Hacer llamada de prueba::

  >>> User = Model.get('res.user')
  >>> users = User.find()
  >>> users[0].login
  'admin'
  >>> admin = users[0]
  >>> admin.phone_extension = '120'
  >>> admin.save()

  >>> test_configuration = Wizard('tryphone.test_configuration', [])
  >>> test_configuration.form.to = '8099'
  >>> test_configuration.execute('try_call')
  >>> CallLog = Model.get('tryphone.call_log')
  >>> call_logs = CallLog.find()
  >>> call_logs[0].destination
  '8099'
