import unittest

from tryphone.provider.freetwitch import FreetwitchProvider
from tryphone.provider.config import ProviderConfiguration


class FreetwitchProviderTest(unittest.TestCase):
    def setUp(self):
        self.configuration = ProviderConfiguration()
        self.configuration.with_('host', 'localhost')
        self.configuration.with_('port', '5099')
        self.configuration.with_('username', 'user')
        self.configuration.with_('password', 'password')

    def test_instanciate_provider(self):
        provider = FreetwitchProvider(self.configuration)
        self.assertIsInstance(provider, FreetwitchProvider)
