from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool


class TestCallerUnit(ModuleTestCase):
    "Tryphone caller tests"

    module = "tryphone"

    @with_transaction()
    def test_make_call(self):
        pool = Pool()
        Caller = pool.get('tryphone.caller')

        self._config_tryphone_api_to_test()

        destination = '2032'
        test_reference_call = 'test'
        admin = self._get_user('admin')

        Caller.call(
            user=admin,
            destination=destination
        )
        logs = self._get_call_log(admin, destination)
        self.assertGreater(len(logs), 0)

        log = logs[0]
        self.assertEqual(log.destination, destination)
        self.assertEqual(log.reference_id, test_reference_call)

    def _add_extension_to_user(self, login='admin', extension='1001'):
        pool = Pool()
        User = pool.get('res.user')
        user = User.search(['login', '=', login])[0]
        user.phone_extension = extension
        user.save()

    def _get_user(self, login):
        pool = Pool()
        User = pool.get('res.user')
        return User.search(['login', '=', login])[0]

    def _config_tryphone_api_to_test(self):
        pool = Pool()
        Configuration = pool.get('tryphone.configuration')

        configuration = Configuration()
        configuration.api_client = 'test'
        configuration.host = '127.0.0.1'
        configuration.port = '8090'
        configuration.save()

    def _get_call_log(self, user, destination):
        pool = Pool()
        CallLog = pool.get('tryphone.call_log')
        return CallLog.search([
            ('user', '=', user),
            ('destination', '=', destination)
        ])


del ModuleTestCase
