# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import PoolMeta
from trytond.model import fields


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    phone_extension = fields.Char('Phone extension', required=False)
